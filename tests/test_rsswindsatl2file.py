'''
Created on 12 feb. 2016

@author: jfpiolle
'''
import os

from cerbere.mapper.rsswindsatl2file import RSSWindSatL2File
from cerbere.datamodel.grid import Grid
from cerbere.mapper.ncfile import NCFile
import matplotlib.pyplot as plt

fname = "/home/cerdata/provider/remss/windsat/bmaps_v07/validated/data/2016/013/wsat_20160113v7.0.1.asc"

f = RSSWindSatL2File(fname)
f.open()
print "field names : "
fields = f.get_fieldnames()
print fields

for field in fields:
    print f.read_field(field)

print f.get_start_time()
print f.get_end_time()


print f.read_values('wdir', slices=[slice(10,20,1), slice(10,20,1)])

data = f.read_values('w-aw')
print data.shape

plt.imshow(data)
plt.show()
f.close()

# convert to netcdf file
g = Grid()
f = RSSWindSatL2File(fname)
g.load(f)
g2 = g.extract_subset()
if os.path.exists('windsat.nc'):
    os.remove('windsat.nc')
outncf = NCFile('windsat.nc', "w")
g2.save(outncf)
