# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

long_desc = '''
This package contains the Cerbere extension for REMSS file formats.

'''

#requires = ['cerbere>=0.1.1']

setup(
    name='cerberecontrib-rss',
    version='0.1',
    url='',
    download_url='',
    license='GPLv3',
    author='Jeff Piolle',
    author_email='jfpiolle@gmail.com',
    description='Cerbere extension for REMSS formats',
    long_description=long_desc,
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GPLv3 License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Documentation',
        'Topic :: Utilities',
    ],
    platforms='any',
    packages=find_packages(),
    include_package_data=True,
#    install_requires=requires,
    namespace_packages=['cerbere'],
)
