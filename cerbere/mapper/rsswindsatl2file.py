# -*- coding: utf-8 -*-
"""
.. module::cerbere.mapper.rsswindsatl2file

Mapper class for RSS Windsat L2 format. Uses some code
provided by RSS.

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from collections import OrderedDict
import os
from copy import copy
from datetime import datetime, timedelta

import numpy
from netCDF4 import default_fillvals

from cerbere.mapper.abstractmapper import AbstractMapper
from cerbere.mapper.slices import Slices, format_slices
from cerbere.datamodel.field import Field
from cerbere.datamodel.variable import Variable
from bytemaps import Dataset


class WindSatDaily(Dataset):
    def __init__(self, filename, missing=-999.):
        """
        Required arguments:
            filename = name of data file to be read (string)

        Optional arguments:
            missing = fill value for missing data,
                      default is the value used in verify file
        """
        self.filename = filename
        self.missing = missing
        Dataset.__init__(self)

    def _attributes(self):
        return ['coordinates', 'long_name', 'units',
                'valid_min', 'valid_max']

    def _coordinates(self):
        return ('orbit_segment', 'variable', 'latitude', 'longitude')

    def _shape(self):
        return (2, 9, 720, 1440)

    def _variables(self):
        return ['time', 'sst', 'w-lf', 'w-mf', 'vapor', 'cloud', 'rain',
                'w-aw', 'wdir', 'land', 'ice']

    def _get_dtype(self, var):
        return {'time': 'f4',
                'sst': 'f8',
                'w-lf': 'f8',
                'w-mf': 'f8',
                'vapor': 'f8',
                'cloud': 'f8',
                'rain': 'f8',
                'w-aw': 'f8',
                'wdir': 'f8',
                'lon': 'f8',
                'lat': 'f8',
                'land': 'i1',
                'ice': 'i1',
                'nodata': 'i1',
                }[var]

    def _get_index(self, var):
        return {'time': 0,
                'sst': 1,
                'w-lf': 2,
                'w-mf': 3,
                'vapor': 4,
                'cloud': 5,
                'rain': 6,
                'w-aw': 7,
                'wdir': 8,
                }[var]

    def _get_scale(self, var):
        scales = {'time': 6.,
                  'sst': 0.15,
                  'w-lf': 0.2,
                  'w-mf': 0.2,
                  'vapor': 0.3,
                  'cloud': 0.01,
                  'rain': 0.1,
                  'w-aw': 0.2,
                  'wdir': 1.5,
                  }
        if var in scales:
            return scales[var]
        else:
            return 1

    def _get_offset(self, var):
        offsets = {'sst': -3.0,
                   'cloud': -0.05,
                   }
        if var in offsets:
            return offsets[var]
        else:
            return 0

    # _get_ attributes:
    def _get_long_name(self, var):
        return {'time': 'pixel time in UTC',
                'sst': 'Sea Surface Temperature',
                'w-lf': '10-m Surface Wind Speed (low frequency)',
                'w-mf': '10-m Surface Wind Speed (medium frequency)',
                'vapor': 'Columnar Water Vapor',
                'cloud': 'Cloud Liquid Water',
                'rain': 'Surface Rain Rate',
                'w-aw': 'All-Weather 10-m Surface Wind Speed',
                'wdir': 'Surface Wind Direction',
                'lon': 'Grid Cell Center Longitude',
                'lat': 'Grid Cell Center Latitude',
                'land': 'Is this land?',
                'ice': 'Is this ice?',
                'nodata': 'Is there no data?',
                }[var]

    def _get_units(self, var):
        return {'time': 'seconds since 2000-01-01 00:00:00',
                'sst': 'deg Celsius',
                'w-lf': 'm s-1',
                'w-mf': 'm s-1',
                'vapor': 'mm',
                'cloud': 'mm',
                'rain': 'mm hr-1',
                'w-aw': 'm s-1',
                'wdir': 'deg oceanographic',
                'lon': 'degrees_east',
                'lat': 'degrees_north',
                'land': 'True or False',
                'ice': 'True or False',
                'nodata': 'True or False',
                }[var]

    def _get_valid_min(self, var):
        return {'time': 0.0,
                'sst': -3.0,
                'w-lf': 0.0,
                'w-mf': 0.0,
                'vapor': 0.0,
                'cloud': -0.05,
                'rain': 0.0,
                'w-aw': 0.2,
                'wdir': 0.0,
                'lon': 0.0,
                'lat': -90.0,
                'land': 0,
                'ice': 0,
                'nodata': 0,
                }[var]

    def _get_valid_max(self, var):
        return {'time': 1440.0,
                'sst': 34.5,
                'w-lf': 50.0,
                'w-mf': 50.0,
                'vapor': 75.0,
                'cloud': 2.45,
                'rain': 25.0,
                'w-aw': 50.0,
                'wdir': 360.0,
                'lon': 360.0,
                'lat': 90.0,
                'land': 1,
                'ice': 1,
                'nodata': 1,
                }[var]


class RSSWindSatL2File(AbstractMapper):
    """
    The RSS files contain both ascending and descending passes in two separate
    grids. You must specify which grid you want to read either by:
      * adding '.asc' or '.desc' to the filename
      * using passdir argument, with 'asc' or 'desc' value

    Args:
        passdir ('asc' or 'desc'): passes read from the file (ascending or
            descending)
    """
    DIMENSIONS = OrderedDict([('y', 720),
                              ('x', 1440)])
    PASS = {'asc': 0, 'desc': 1}
    TIMEORIGIN = datetime(2000, 1, 1)

    def __init__(self, url=None, passdir=None, **kwargs):
        """
        """
        super(RSSWindSatL2File, self).__init__(
            url=url.strip('.asc').strip('.desc'),
            **kwargs)
        if url.endswith('.asc'):
            self.passdir = self.PASS['asc']
        elif url.endswith('.desc'):
            self.passdir = self.PASS['desc']
        elif passdir is not None:
            self.passdir = self.PASS[passdir]
        else:
            raise Exception("The type of passes to be read must be specified")
        self.__globalattributes = None
        self._startdate = datetime.strptime(
            os.path.basename(url)[5:13],
            "%Y%m%d"
            )
        return

    def open(self, view=None, datamodel=None, datamodel_geolocation_dims=None):
        """Open the file (or any other type of storage)

        Args:
            view (dict, optional): a dictionary where keys are dimension names
                and values are slices. A view can be set on a file, meaning
                that only the subset defined by this view will be accessible.
                This view is expressed as any subset (see :func:`get_values`).
                For example:

                view = {'time':slice(0,0), 'lat':slice(200,300),
                'lon':slice(200,300)}

            datamodel (str): type of feature read or written. Internal argument
                only used by the classes from :mod:`~cerbere.datamodel`
                package. Can be 'Grid', 'Swath', etc...

            datamodel_geolocation_dims (list, optional): list of the name of
                the geolocation dimensions defining the data model to be read
                in the file. Optional argument, only used by the datamodel
                classes, in case the mapper class can store different types of
                data models.

        Returns:
            a handler on the opened file
        """
        dims = self.DIMENSIONS.values()
        dims.insert(0, 2)
        newview = Slices(format_slices(view, self.DIMENSIONS.keys()),
                         self.DIMENSIONS.values())
        newview = Slices((slice(self.passdir, self.passdir + 1),) + newview,
                         dims)
        super(RSSWindSatL2File, self).open(newview, datamodel,
                                           datamodel_geolocation_dims)
        self._handler = WindSatDaily(self._url)

    def close(self):
        """Close handler on storage"""
        pass

    def __check_open(self):
        if self._handler is None:
            raise IOError('File is not open')

    def get_matching_dimname(self, dimname):
        """Return the equivalent name in the native format for a standard
        dimension.

        This is a translation of the standard names to native ones. It is used
        for internal purpose only and should not be called directly.

        The standard dimension names are:

        * x, y, time for :class:`~cerbere.datamodel.grid.Grid`
        * row, cell, time for :class:`~cerbere.datamodel.swath.Swath` or
          :class:`~cerbere.datamodel.image.Image`

        To be derived when creating an inherited data mapper class. This is
        mandatory for geolocation dimensions which must be standard.

        Args:
            dimname (str): standard dimension name.

        Returns:
            str: return the native name for the dimension. Return `dimname` if
                the input dimension has no standard name.

        See Also:
            see :func:`get_standard_dimname` for the reverse operation
        """
        return dimname

    def get_standard_dimname(self, dimname):
        """
        Returns the equivalent standard dimension name for a
        dimension in the native format.

        This is a translation of the native names to standard ones. It is used
        for internal purpose and should not be called directly.

        To be derived when creating an inherited data mapper class. This is
        mandatory for geolocation dimensions which must be standard.

        Args:
            dimname (string): native dimension name

        Return:
            str: the (translated) standard name for the dimension. Return
            `dimname` if the input dimension has no standard name.

        See Also:
            see :func:`get_matching_dimname` for the reverse operation
        """
        return dimname

    def get_dimensions(self, fieldname=None):
        """Return the dimension's standard names of a file or a field in the
        file.

        Args:
            fieldname (str): the name of the field from which to get the
                dimensions. For a geolocation field, use the cerbere standard
                name (time, lat, lon), though native field name will work too.

        Returns:
            tuple<str>: the standard dimensions of the field or file.
        """
        if fieldname == 'lat':
            return ('y',)
        if fieldname == 'lon':
            return ('x',)
        return ('y', 'x')

    def get_fieldnames(self):
        """Returns the list of geophysical fields stored for the feature.

        The geolocation field names are excluded from this list.

        Returns:
            list<string>: list of field names
        """
        self.__check_open()
        return self._handler._variables()[1:]

    def get_dimsize(self, dimname):
        """Return the size of a dimension.

        Args:
            dimname (str): name of the dimension.

        Returns:
            int: size of the dimension.
        """
        shape = self.view.shape()
        dimindex = {'y': 1, 'x': 2}[dimname]
        return shape[dimindex]

    def get_geolocation_field(self, fieldname):
        """Return the equivalent field name in the file format for a standard
        geolocation field (lat, lon, time, z).

        Used for internal purpose and should not be called directly.

        Args:
            fieldname (str): name of the standard geolocation field (lat, lon
                or time)

        Return:
            str: name of the corresponding field in the native file format.
                Returns None if no matching is found
        """
        return fieldname

    def read_field(self, fieldname):
        """
        Return the :class:`cerbere.field.Field` object corresponding to
        the requested fieldname.

        The :class:`cerbere.field.Field` class contains all the metadata
        describing a field (equivalent to a variable in netCDF).

        Args:
            fieldname (str): name of the field

        Returns:
            :class:`cerbere.field.Field`: the corresponding field object
        """
        self.__check_open()
        field = Field(
            variable=Variable(
                shortname=fieldname,
                description=self._handler._get_long_name(fieldname)
                ),
            units=self._handler._get_units(fieldname),
            datatype=numpy.dtype(self._handler._get_dtype(fieldname)),
            valid_min=self._handler._get_valid_min(fieldname),
            valid_max=self._handler._get_valid_max(fieldname),
            dimensions=self.get_full_dimensions(fieldname),
            fillvalue=self.read_fillvalue(fieldname)
        )
        field.attach_storage(self.get_field_handler(fieldname))
        return field

    def read_values(self, fieldname, slices=None, **kwargs):
        """Read the data of a field.

        Args:
            fieldname (str): name of the field which to read the data from

            slices (list of slice, optional): list of slices for the field if
                subsetting is requested. A slice must then be provided for each
                field dimension. The slices are relative to the opened view
                (see :func:open) if a view was set when opening the file.

        Return:
            MaskedArray: array of data read. Array type is the same as the
                storage type.
        """
        self.__check_open()
        # merge slices
        dims = [self.get_dimsize(dim)
                for dim in self.get_dimensions(fieldname)]
        absslices = self.view
        if fieldname in ['lat', 'lon']:
            if fieldname == 'lat':
                absslices = Slices((absslices[1],),
                                   self.get_dimsize('y'))
            else:
                absslices = Slices((absslices[2],),
                                   self.get_dimsize('x'))
            if slices is not None:
                newslices = copy(slices)
                newslices = Slices(slices, dims)
                absslices = newslices.absolute_slices(absslices)
        else:
            dims.insert(0, 1)
            if slices is not None:
                newslices = copy(slices)
                newslices.insert(0, slice(0, 1))
                newslices = Slices(newslices, dims)
                absslices = newslices.absolute_slices(absslices)

        # read data
        if fieldname == 'lat':
            return numpy.arange(-89.875, 90.125, 0.25)[absslices]
        if fieldname == 'lon':
            return numpy.arange(-179.875, 180.125, 0.25)[absslices]

        # center data on greenwhich
        data = numpy.roll(self._handler.variables[fieldname], 720, axis=2)

        # mask fill values
        data = numpy.ma.masked_equal(
                data[absslices],
                self._handler.missing).squeeze(0)

        if fieldname == 'time':
            data = (data * 60 +
                    (self._startdate - self.TIMEORIGIN).total_seconds())
        return data

    def read_fillvalue(self, fieldname):
        """Read the fill value of a field.

        Args:
            fieldname (str): name of the field.

        Returns:
            number or char or str: fill value of the field. The type is the
                as the type of the data in the field.
        """
        return default_fillvals[self._handler._get_dtype(fieldname)]

    def read_field_attributes(self, fieldname):
        """Return the specific attributes of a field.

        Args:
            fieldname (str): name of the field.

        Returns:
            dict<string, string or number or datetime>: a dictionary where keys
                are the attribute names.
        """
        return {}

    def read_global_attributes(self):
        """Returns the names of the global attributes.

        Returns:
            list<str>: the list of the attribute names.
        """
        return {}

    def read_global_attribute(self, name):
        """Returns the value of a global attribute.

        Args:
            name (str): name of the global attribute.

        Returns:
            str, number or datetime: value of the corresponding attribute.
        """
        raise ValueError("Attribute {} is not existing".format(name))

    def get_start_time(self):
        """Returns the minimum date of the file temporal coverage.

        Returns:
            datetime: start time of the data in file.
        """
        return (self.TIMEORIGIN +
                timedelta(seconds=self.read_values('time').min()))

    def get_end_time(self):
        """Returns the maximum date of the file temporal coverage.

        Returns:
            datetime: end time of the data in file.
        """
        return (self.TIMEORIGIN +
                timedelta(seconds=self.read_values('time').max()))

    def get_bbox(self):
        """Returns the bounding box of the feature, as a tuple.

        Returns:
            tuple: bbox expressed as (lonmin, latmin, lonmax, latmax)
        """
        return (-180., -90., 180., 90.)

    def create_field(self, field, dim_translation=None):
        """Creates a new field in the mapper.

        Creates the field structure but don't write yet its values array.

        Args:
            field (Field): the field to be created.

        See also:
            :func:`write_field` for writing the values array.
        """
        raise NotImplementedError

    def create_dim(self, dimname, size=None):
        """Add a new dimension.

        Args:
            dimname (str): name of the dimension.
            size (int): size of the dimension (unlimited if None)
        """
        raise NotImplementedError

    def write_field(self, fieldname):
        """Writes the field data on disk.

        Args:
            fieldname (str): name of the field to write.
        """
        raise NotImplementedError

    def write_global_attributes(self, attrs):
        """Write the global attributes of the file.

        Args:
            attrs (dict<string, string or number or datetime>): a dictionary
                containing the attributes names and values to be written.
        """
        raise NotImplementedError
